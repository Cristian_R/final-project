Template.liked.helpers({
likes: function () {
        if (Meteor.user().profile) {
            var likes = Meteor.user().profile.likes;
            var likesList = [];

            for (var i = 0; i < likes.length; i++) {
                likesList.push(Meteor.users.findOne(likes[i]));
            }
            return likesList;
        }
    }
});