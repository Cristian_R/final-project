Template.profile.events({
    'submit .nouvelle-message': function (event) {
        event.preventDefault();

        var postText = event.currentTarget.children[0].firstElementChild.value;
        Collections.Posts.insert({
            name: postText,
            createdAt: new Date(),
            username: Meteor.user().username,
        });
        event.currentTarget.children[0].firstElementChild.value = "";
    }
});
Template.profile.helpers({
 postList: function(){
        return Collections.Posts.find({});
    },
});